@[Link(ldflags: "`command -v pkg-config > /dev/null && pkg-config --libs ddjvuapi 2> /dev/null|| printf %s ''`")]
lib LibDjvu
  type MiniexpT = Void*
end

require "./ddjvu_api.cr"
require "./miniexp.cr"
