require "./lib_djvu"

@[Link(ldflags: "`command -v pkg-config > /dev/null && pkg-config --libs ddjvuapi 2> /dev/null|| printf %s ''`")]
lib LibDjvu
  $miniexp_io : MiniexpIoT
  $miniexp_macrochar : MiniexpMacrocharT[128]
  $miniexp_macroqueue : MinivarT
  MINIEXP_IO_PRINT7BITS       =  1
  MINIEXP_IO_QUOTEMORESYMBOLS = 32
  MINIEXP_IO_U4ESCAPE         =  2
  MINIEXP_IO_U6ESCAPE         =  4
  MINIEXP_NIL               =  0_u64.unsafe_as(MiniexpT)
  MINIEXP_DUMMY               =  2_u64.unsafe_as(MiniexpT)
  alias MiniexpMacrocharT = (MiniexpIoT* -> MiniexpT)
  alias MiniexpS = Void
  alias MinivarS = Void
  fun miniexp_caar(p : MiniexpT) : MiniexpT
  fun miniexp_caddr(p : MiniexpT) : MiniexpT
  fun miniexp_cadr(p : MiniexpT) : MiniexpT
  fun miniexp_car(p : MiniexpT) : MiniexpT
  fun miniexp_cdar(p : MiniexpT) : MiniexpT
  fun miniexp_cdddr(p : MiniexpT) : MiniexpT
  fun miniexp_cddr(p : MiniexpT) : MiniexpT
  fun miniexp_cdr(p : MiniexpT) : MiniexpT
  fun miniexp_classof(p : MiniexpT) : MiniexpT
  fun miniexp_concat(l : MiniexpT) : MiniexpT
  fun miniexp_cons(car : MiniexpT, cdr : MiniexpT) : MiniexpT
  fun miniexp_consp(p : MiniexpT) : LibC::Int
  fun miniexp_double(x : LibC::Double) : MiniexpT
  fun miniexp_doublep(p : MiniexpT) : LibC::Int
  fun miniexp_floatnum(x : LibC::Double) : MiniexpT
  fun miniexp_floatnump(p : MiniexpT) : LibC::Int
  fun miniexp_io_init(io : MiniexpIoT*)
  fun miniexp_isa(p : MiniexpT, c : MiniexpT) : MiniexpT
  fun miniexp_length(p : MiniexpT) : LibC::Int
  fun miniexp_listp(p : MiniexpT) : LibC::Int
  fun miniexp_nth(n : LibC::Int, l : MiniexpT) : MiniexpT
  fun miniexp_number(x : LibC::Int) : MiniexpT
  fun miniexp_numberp(p : MiniexpT) : LibC::Int
  fun miniexp_objectp(p : MiniexpT) : LibC::Int
  fun miniexp_pname(p : MiniexpT, width : LibC::Int) : MiniexpT
  fun miniexp_pprin(p : MiniexpT, width : LibC::Int) : MiniexpT
  fun miniexp_pprin_r(io : MiniexpIoT*, p : MiniexpT, w : LibC::Int) : MiniexpT
  fun miniexp_pprint(p : MiniexpT, width : LibC::Int) : MiniexpT
  fun miniexp_pprint_r(io : MiniexpIoT*, p : MiniexpT, w : LibC::Int) : MiniexpT
  fun miniexp_prin(p : MiniexpT) : MiniexpT
  fun miniexp_prin_r(io : MiniexpIoT*, p : MiniexpT) : MiniexpT
  fun miniexp_print(p : MiniexpT) : MiniexpT
  fun miniexp_print_r(io : MiniexpIoT*, p : MiniexpT) : MiniexpT
  fun miniexp_read : MiniexpT
  fun miniexp_read_r(io : MiniexpIoT*) : MiniexpT
  fun miniexp_reverse(p : MiniexpT) : MiniexpT
  fun miniexp_rplaca(pair : MiniexpT, newcar : MiniexpT) : MiniexpT
  fun miniexp_rplacd(pair : MiniexpT, newcdr : MiniexpT) : MiniexpT
  fun miniexp_string(s : LibC::Char*) : MiniexpT
  fun miniexp_stringp(p : MiniexpT) : LibC::Int
  fun miniexp_substring(s : LibC::Char*, n : LibC::Int) : MiniexpT
  fun miniexp_symbol(name : LibC::Char*) : MiniexpT
  fun miniexp_symbolp(p : MiniexpT) : LibC::Int
  fun miniexp_to_double(p : MiniexpT) : LibC::Double
  fun miniexp_to_int(p : MiniexpT) : LibC::Int
  fun miniexp_to_name(p : MiniexpT) : LibC::Char*
  fun miniexp_to_str(p : MiniexpT) : LibC::Char*

  struct MiniexpIoS
    fputs : (MiniexpIoT*, LibC::Char* -> LibC::Int)
    fgetc : (MiniexpIoT* -> LibC::Int)
    ungetc : (MiniexpIoT*, LibC::Int -> LibC::Int)
    data : Void*[4]
    p_flags : LibC::Int*
    p_macrochar : MiniexpMacrocharT*
    p_diezechar : MiniexpMacrocharT*
    p_macroqueue : MinivarT
    p_reserved : MinivarT
  end

  type MiniexpIoT = MiniexpIoS*
  type MinivarT = MinivarS*
end
