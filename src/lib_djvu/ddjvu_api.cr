require "./lib_djvu"

@[Link(ldflags: "`command -v pkg-config > /dev/null && pkg-config --libs ddjvuapi 2> /dev/null|| printf %s ''`")]
lib LibDjvu
  alias DdjvuContextS = Void
  alias DdjvuDocumentS = Void
  alias DdjvuFormatS = Void
  alias DdjvuJobS = Void
  alias DdjvuMessageCallbackT = (DdjvuContextT, Void* -> Void)
  alias DdjvuPageS = Void
  alias DdjvuRectmapperS = Void
  alias X_IoCodecvt = Void
  alias X_IoLockT = Void
  alias X_IoMarker = Void
  alias X_IoWideData = Void
  alias X__Off64T = LibC::Long
  alias X__OffT = LibC::Long
  enum DdjvuDocumentTypeT
    DdjvuDoctypeUnknown    = 0
    DdjvuDoctypeSinglepage = 1
    DdjvuDoctypeBundled    = 2
    DdjvuDoctypeIndirect   = 3
    DdjvuDoctypeOldBundled = 4
    DdjvuDoctypeOldIndexed = 5
  end
  enum DdjvuFormatStyleT
    DdjvuFormatBgr24     = 0
    DdjvuFormatRgb24     = 1
    DdjvuFormatRgbmask16 = 2
    DdjvuFormatRgbmask32 = 3
    DdjvuFormatGrey8     = 4
    DdjvuFormatPalette8  = 5
    DdjvuFormatMsbtolsb  = 6
    DdjvuFormatLsbtomsb  = 7
  end
  enum DdjvuMessageTagT
    DdjvuError     = 0
    DdjvuInfo      = 1
    DdjvuNewstream = 2
    DdjvuDocinfo   = 3
    DdjvuPageinfo  = 4
    DdjvuRelayout  = 5
    DdjvuRedisplay = 6
    DdjvuChunk     = 7
    DdjvuThumbnail = 8
    DdjvuProgress  = 9
  end
  enum DdjvuPageRotationT
    DdjvuRotate0   = 0
    DdjvuRotate90  = 1
    DdjvuRotate180 = 2
    DdjvuRotate270 = 3
  end
  enum DdjvuPageTypeT
    DdjvuPagetypeUnknown  = 0
    DdjvuPagetypeBitonal  = 1
    DdjvuPagetypePhoto    = 2
    DdjvuPagetypeCompound = 3
  end
  enum DdjvuRenderModeT
    DdjvuRenderColor      = 0
    DdjvuRenderBlack      = 1
    DdjvuRenderColoronly  = 2
    DdjvuRenderMaskonly   = 3
    DdjvuRenderBackground = 4
    DdjvuRenderForeground = 5
  end
  enum DdjvuStatusT
    DdjvuJobNotstarted = 0
    DdjvuJobStarted    = 1
    DdjvuJobOk         = 2
    DdjvuJobFailed     = 3
    DdjvuJobStopped    = 4
  end
  fun ddjvu_anno_get_bgcolor(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_horizalign(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_hyperlinks(annotations : MiniexpT) : MiniexpT*
  fun ddjvu_anno_get_metadata(annotations : MiniexpT, key : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_metadata_keys(annotations : MiniexpT) : MiniexpT*
  fun ddjvu_anno_get_mode(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_vertalign(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_xmp(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_anno_get_zoom(annotations : MiniexpT) : LibC::Char*
  fun ddjvu_cache_clear(context : DdjvuContextT)
  fun ddjvu_cache_get_size(context : DdjvuContextT) : LibC::ULong
  fun ddjvu_cache_set_size(context : DdjvuContextT, cachesize : LibC::ULong)
  fun ddjvu_code_get_version : LibC::Int
  fun ddjvu_context_create(programname : LibC::Char*) : DdjvuContextT
  fun ddjvu_context_release(context : DdjvuContextT)
  fun ddjvu_document_check_pagedata(document : DdjvuDocumentT, pageno : LibC::Int) : LibC::Int
  fun ddjvu_document_create(context : DdjvuContextT, url : LibC::Char*, cache : LibC::Int) : DdjvuDocumentT
  fun ddjvu_document_create_by_filename(context : DdjvuContextT, filename : LibC::Char*, cache : LibC::Int) : DdjvuDocumentT
  fun ddjvu_document_create_by_filename_utf8(context : DdjvuContextT, filename : LibC::Char*, cache : LibC::Int) : DdjvuDocumentT
  fun ddjvu_document_get_anno(document : DdjvuDocumentT, compat : LibC::Int) : MiniexpT
  fun ddjvu_document_get_filedump(document : DdjvuDocumentT, fileno : LibC::Int) : LibC::Char*
  fun ddjvu_document_get_fileinfo_imp(document : DdjvuDocumentT, fileno : LibC::Int, info : DdjvuFileinfoT*, infosz : LibC::UInt) : DdjvuStatusT
  fun ddjvu_document_get_filenum(document : DdjvuDocumentT) : LibC::Int
  fun ddjvu_document_get_outline(document : DdjvuDocumentT) : MiniexpT
  fun ddjvu_document_get_pageanno(document : DdjvuDocumentT, pageno : LibC::Int) : MiniexpT
  fun ddjvu_document_get_pagedump(document : DdjvuDocumentT, pageno : LibC::Int) : LibC::Char*
  fun ddjvu_document_get_pageinfo_imp(document : DdjvuDocumentT, pageno : LibC::Int, info : DdjvuPageinfoT*, infosz : LibC::UInt) : DdjvuStatusT
  fun ddjvu_document_get_pagenum(document : DdjvuDocumentT) : LibC::Int
  fun ddjvu_document_get_pagetext(document : DdjvuDocumentT, pageno : LibC::Int, maxdetail : LibC::Char*) : MiniexpT
  fun ddjvu_document_get_type(document : DdjvuDocumentT) : DdjvuDocumentTypeT
  fun ddjvu_document_job(document : DdjvuDocumentT) : DdjvuJobT
  fun ddjvu_document_print(document : DdjvuDocumentT, output : File*, optc : LibC::Int, optv : LibC::Char**) : DdjvuJobT
  fun ddjvu_document_save(document : DdjvuDocumentT, output : File*, optc : LibC::Int, optv : LibC::Char**) : DdjvuJobT
  fun ddjvu_document_search_pageno(x0 : DdjvuDocumentT, x1 : LibC::Char*) : LibC::Int
  fun ddjvu_format_create(style : DdjvuFormatStyleT, nargs : LibC::Int, args : LibC::UInt*) : DdjvuFormatT
  fun ddjvu_format_release(format : DdjvuFormatT)
  fun ddjvu_format_set_ditherbits(format : DdjvuFormatT, bits : LibC::Int)
  fun ddjvu_format_set_gamma(format : DdjvuFormatT, gamma : LibC::Double)
  fun ddjvu_format_set_row_order(format : DdjvuFormatT, top_to_bottom : LibC::Int)
  fun ddjvu_format_set_white(format : DdjvuFormatT, b : UInt8, g : UInt8, r : UInt8)
  fun ddjvu_format_set_y_direction(format : DdjvuFormatT, top_to_bottom : LibC::Int)
  fun ddjvu_get_version_string : LibC::Char*
  fun ddjvu_job_get_user_data(job : DdjvuJobT) : Void*
  fun ddjvu_job_release(job : DdjvuJobT)
  fun ddjvu_job_set_user_data(job : DdjvuJobT, userdata : Void*)
  fun ddjvu_job_status(job : DdjvuJobT) : DdjvuStatusT
  fun ddjvu_job_stop(job : DdjvuJobT)
  fun ddjvu_map_point(mapper : DdjvuRectmapperT, x : LibC::Int*, y : LibC::Int*)
  fun ddjvu_map_rect(mapper : DdjvuRectmapperT, rect : DdjvuRectT*)
  fun ddjvu_message_peek(context : DdjvuContextT) : DdjvuMessageT*
  fun ddjvu_message_pop(context : DdjvuContextT)
  fun ddjvu_message_set_callback(context : DdjvuContextT, callback : DdjvuMessageCallbackT, closure : Void*)
  fun ddjvu_message_wait(context : DdjvuContextT) : DdjvuMessageT*
  fun ddjvu_miniexp_release(document : DdjvuDocumentT, expr : MiniexpT)
  fun ddjvu_page_create_by_pageid(document : DdjvuDocumentT, pageid : LibC::Char*) : DdjvuPageT
  fun ddjvu_page_create_by_pageno(document : DdjvuDocumentT, pageno : LibC::Int) : DdjvuPageT
  fun ddjvu_page_get_gamma(page : DdjvuPageT) : LibC::Double
  fun ddjvu_page_get_height(page : DdjvuPageT) : LibC::Int
  fun ddjvu_page_get_initial_rotation(page : DdjvuPageT) : DdjvuPageRotationT
  fun ddjvu_page_get_long_description(x0 : DdjvuPageT) : LibC::Char*
  fun ddjvu_page_get_resolution(page : DdjvuPageT) : LibC::Int
  fun ddjvu_page_get_rotation(page : DdjvuPageT) : DdjvuPageRotationT
  fun ddjvu_page_get_short_description(x0 : DdjvuPageT) : LibC::Char*
  fun ddjvu_page_get_type(page : DdjvuPageT) : DdjvuPageTypeT
  fun ddjvu_page_get_version(page : DdjvuPageT) : LibC::Int
  fun ddjvu_page_get_width(page : DdjvuPageT) : LibC::Int
  fun ddjvu_page_job(page : DdjvuPageT) : DdjvuJobT
  fun ddjvu_page_render(page : DdjvuPageT, mode : DdjvuRenderModeT, pagerect : DdjvuRectT*, renderrect : DdjvuRectT*, pixelformat : DdjvuFormatT, rowsize : LibC::ULong, imagebuffer : LibC::Char*) : LibC::Int
  fun ddjvu_page_set_rotation(page : DdjvuPageT, rot : DdjvuPageRotationT)
  fun ddjvu_rectmapper_create(input : DdjvuRectT*, output : DdjvuRectT*) : DdjvuRectmapperT
  fun ddjvu_rectmapper_modify(mapper : DdjvuRectmapperT, rotation : LibC::Int, mirrorx : LibC::Int, mirrory : LibC::Int)
  fun ddjvu_rectmapper_release(mapper : DdjvuRectmapperT)
  fun ddjvu_stream_close(document : DdjvuDocumentT, streamid : LibC::Int, stop : LibC::Int)
  fun ddjvu_stream_write(document : DdjvuDocumentT, streamid : LibC::Int, data : LibC::Char*, datalen : LibC::ULong)
  fun ddjvu_thumbnail_render(document : DdjvuDocumentT, pagenum : LibC::Int, wptr : LibC::Int*, hptr : LibC::Int*, pixelformat : DdjvuFormatT, rowsize : LibC::ULong, imagebuffer : LibC::Char*) : LibC::Int
  fun ddjvu_thumbnail_status(document : DdjvuDocumentT, pagenum : LibC::Int, start : LibC::Int) : DdjvuStatusT
  fun ddjvu_unmap_point(mapper : DdjvuRectmapperT, x : LibC::Int*, y : LibC::Int*)
  fun ddjvu_unmap_rect(mapper : DdjvuRectmapperT, rect : DdjvuRectT*)

  struct DdjvuFileinfoS
    type : LibC::Char
    pageno : LibC::Int
    size : LibC::Int
    id : LibC::Char*
    name : LibC::Char*
    title : LibC::Char*
  end

  struct DdjvuMessageAnyS
    tag : DdjvuMessageTagT
    context : DdjvuContextT
    document : DdjvuDocumentT
    page : DdjvuPageT
    job : DdjvuJobT
  end

  struct DdjvuMessageChunkS
    any : DdjvuMessageAnyT
    chunkid : LibC::Char*
  end

  struct DdjvuMessageDocinfoS
    any : DdjvuMessageAnyT
  end

  struct DdjvuMessageErrorS
    any : DdjvuMessageAnyT
    message : LibC::Char*
    function : LibC::Char*
    filename : LibC::Char*
    lineno : LibC::Int
  end

  struct DdjvuMessageInfoS
    any : DdjvuMessageAnyT
    message : LibC::Char*
  end

  struct DdjvuMessageNewstreamS
    any : DdjvuMessageAnyT
    streamid : LibC::Int
    name : LibC::Char*
    url : LibC::Char*
  end

  struct DdjvuMessagePageinfoS
    any : DdjvuMessageAnyT
  end

  struct DdjvuMessageProgressS
    any : DdjvuMessageAnyT
    status : DdjvuStatusT
    percent : LibC::Int
  end

  struct DdjvuMessageRedisplayS
    any : DdjvuMessageAnyT
  end

  struct DdjvuMessageRelayoutS
    any : DdjvuMessageAnyT
  end

  struct DdjvuMessageThumbnailS
    any : DdjvuMessageAnyT
    pagenum : LibC::Int
  end

  struct DdjvuPageinfoS
    width : LibC::Int
    height : LibC::Int
    dpi : LibC::Int
    rotation : LibC::Int
    version : LibC::Int
  end

  struct DdjvuRectS
    x : LibC::Int
    y : LibC::Int
    w : LibC::UInt
    h : LibC::UInt
  end

  struct X_IoFile
    _flags : LibC::Int
    _io_read_ptr : LibC::Char*
    _io_read_end : LibC::Char*
    _io_read_base : LibC::Char*
    _io_write_base : LibC::Char*
    _io_write_ptr : LibC::Char*
    _io_write_end : LibC::Char*
    _io_buf_base : LibC::Char*
    _io_buf_end : LibC::Char*
    _io_save_base : LibC::Char*
    _io_backup_base : LibC::Char*
    _io_save_end : LibC::Char*
    _markers : X_IoMarker*
    _chain : X_IoFile*
    _fileno : LibC::Int
    _flags2 : LibC::Int
    _old_offset : X__OffT
    _cur_column : LibC::UShort
    _vtable_offset : LibC::Char
    _shortbuf : LibC::Char[1]
    _lock : X_IoLockT*
    _offset : X__Off64T
    _codecvt : X_IoCodecvt*
    _wide_data : X_IoWideData*
    _freeres_list : X_IoFile*
    _freeres_buf : Void*
    __pad5 : LibC::SizeT
    _mode : LibC::Int
    _unused2 : LibC::Char[20]
  end

  union DdjvuMessageS
    m_any : DdjvuMessageAnyS
    m_error : DdjvuMessageErrorS
    m_info : DdjvuMessageInfoS
    m_newstream : DdjvuMessageNewstreamS
    m_docinfo : DdjvuMessageDocinfoS
    m_pageinfo : DdjvuMessagePageinfoS
    m_chunk : DdjvuMessageChunkS
    m_relayout : DdjvuMessageRelayoutS
    m_redisplay : DdjvuMessageRedisplayS
    m_thumbnail : DdjvuMessageThumbnailS
    m_progress : DdjvuMessageProgressS
  end

  type DdjvuContextT = Void*
  type DdjvuDocumentT = Void*
  type DdjvuFileinfoT = DdjvuFileinfoS
  type DdjvuFormatT = Void*
  type DdjvuJobT = Void*
  type DdjvuMessageAnyT = DdjvuMessageAnyS
  type DdjvuMessageT = DdjvuMessageS
  type DdjvuPageT = Void*
  type DdjvuPageinfoT = DdjvuPageinfoS
  type DdjvuRectT = DdjvuRectS
  type DdjvuRectmapperT = Void*
  type File = X_IoFile
end
