module Djvu
  class Page
    def initialize(@document : Document, @number : Int32)
      wait_for_decoding
    end

    delegate width, height, resolution, version, to: info

    def text
      result = raw_text("page")
      return if result.nil?
      String.new(LibDjvu.miniexp_to_str(LibDjvu.miniexp_nth(5, result)))
    end

    def html(io : IO)
      result = raw_text("para")
      return if result.nil?
      io << %|<div class="document">|

      (5...LibDjvu.miniexp_length(result)).each do |column_index|
        column = LibDjvu.miniexp_nth(column_index, result)
        io << %|<div class="column">|
        (5...LibDjvu.miniexp_length(column)).each do |para_index|
          para = LibDjvu.miniexp_nth(para_index, column)
          io << %|<p>|
          chars = LibDjvu.miniexp_to_str(LibDjvu.miniexp_nth(5, para))
          io << String.new(chars)
          io << %|</p>|
        end
        io << %|</div>|
      end
      io << %|</div>|
    end

    def html
      String.build { |io| html(io) }
    end

    private def raw_text(max_detail = "max")
      loop do
        result = LibDjvu.ddjvu_document_get_pagetext(
          @document,
          @number,
          max_detail
        )

        case result
        when LibDjvu::MINIEXP_DUMMY
          Context.instance.handle_messages(wait: true)
        when LibDjvu::MINIEXP_NIL then return
        when LibDjvu.miniexp_symbol("failed"), LibDjvu.miniexp_symbol("stopped")
          raise "Failed getting page text"
        else return result
        end
      end
    end

    private def info
      loop do
        result = LibDjvu.ddjvu_document_get_pageinfo_imp(
          @document,
          @number,
          out info,
          sizeof(LibDjvu::DdjvuPageinfoT)
        )

        case result
        when .ddjvu_job_ok? then return info.as(LibDjvu::DdjvuPageinfoS)
        when .ddjvu_job_failed?, .ddjvu_job_stopped?
          raise "Failed to get pageinfo"
        end
      end
    end

    private def wait_for_decoding
      loop do
        result = LibDjvu.ddjvu_document_check_pagedata(@document, @number)
        break if result != 0
        Context.instance.handle_messages(wait: true)
      end
    end
  end
end
