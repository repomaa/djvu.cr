require "../lib_djvu/lib_djvu"
require "./context"
require "./pages"
require "./annotations"

module Djvu
  class Document
    private def initialize(@handler : LibDjvu::DdjvuDocumentT)
    end

    def self.new(filename : String)
      handler = LibDjvu.ddjvu_document_create_by_filename_utf8(
        Context.instance,
        File.expand_path(filename),
        true
      )

      wait_for_decoding(handler)
      new(handler)
    end

    def annotations
      loop do
        annotations = LibDjvu.ddjvu_document_get_anno(self, true)
        case annotations
        when LibDjvu::MINIEXP_DUMMY
          Context.instance.handle_messages(wait: true)
        when LibDjvu::MINIEXP_NIL then return nil
        when LibDjvu.miniexp_symbol("failed"), LibDjvu.miniexp_symbol("stopped")
          raise "Failed to get document annotations"
        else return Annotations.new(annotations)
        end
      end
    end

    def outline
      loop do
        outline = LibDjvu.ddjvu_document_get_outline(self)
        case outline
        when LibDjvu::MINIEXP_DUMMY
          Context.instance.handle_messages(wait: true)
        when LibDjvu::MINIEXP_NIL then return nil
        when LibDjvu.miniexp_symbol("failed"), LibDjvu.miniexp_symbol("stopped")
          raise "Failed to get document outline"
        else
          chars = LibDjvu.miniexp_to_str(outline)
          return chars.try { |pointer| String.new(pointer) }
        end
      end
    end

    def metadata
      annotations.try(&.metadata)
    end

    def text
      pages.map(&.text).join('\n')
    end

    def html
      String.build { |io| html(io) }
    end

    def html(io : IO)
      io << %|<div class="page">|
      pages.each { |page| page.html(io) }
      io << %|</div>|
    end

    def pages
      Pages.new(self)
    end

    def finalize
      LibDjvu.ddjvu_job_release(LibDjvu.ddjvu_document_job(@handler))
    end

    def to_unsafe
      @handler
    end

    private def self.wait_for_decoding(handler)
      loop do
        Context.instance.handle_messages(wait: true)

        case LibDjvu.ddjvu_job_status(LibDjvu.ddjvu_document_job(handler))
        when .ddjvu_job_ok? then break
        when .ddjvu_job_failed?, .ddjvu_job_stopped?
          raise "Failed decoding document"
        end
      end
    end
  end
end
