module Djvu
  class Error < Exception
    def initialize(@error_message : LibDjvu::DdjvuMessageErrorS)
      initialize(format_message(@error_message))
    end

    def format_message(error)
      "%{message} @ %{function} in %{filename}:%{lineno}" % {
        message: String.new(error.message),
        function: String.new(error.function),
        filename: String.new(error.function),
        lineno: error.lineno
      }
    end
  end
end
