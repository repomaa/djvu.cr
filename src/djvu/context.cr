require "../lib_djvu/lib_djvu"
require "./error"

module Djvu
  class Context
    class_getter instance = new

    def initialize
      @handler = LibDjvu.ddjvu_context_create(PROGRAM_NAME)
    end

    def handle_messages(wait = false)
      LibDjvu.ddjvu_message_wait(self) if wait

      loop do
        message_pointer = LibDjvu.ddjvu_message_peek(self)
        break if message_pointer.null?
        message = message_pointer.value.as(LibDjvu::DdjvuMessageS)

        case message.m_any.tag
        when .ddjvu_error? then raise Error.new(message.m_error)
        when .ddjvu_info? then yield message.m_info
        when .ddjvu_newstream? then yield message.m_newstream
        when .ddjvu_docinfo? then yield message.m_docinfo
        when .ddjvu_pageinfo? then yield message.m_pageinfo
        when .ddjvu_relayout? then yield message.m_relayout
        when .ddjvu_redisplay? then yield message.m_redisplay
        when .ddjvu_chunk? then yield message.m_chunk
        when .ddjvu_thumbnail? then yield message.m_thumbnail
        when .ddjvu_progress? then yield message.m_progress
        end

        LibDjvu.ddjvu_message_pop(self)
      end
    end

    def handle_messages(wait = false)
      handle_messages(wait) {}
    end

    def finalize
      LibDjvu.ddjvu_context_release(self)
    end

    def to_unsafe
      @handler
    end
  end
end
