require "./page"

module Djvu
  class Pages
    include Indexable(Page)

    getter size

    def initialize(@document : Document)
      @size = LibDjvu.ddjvu_document_get_pagenum(@document)
    end

    def unsafe_fetch(index : Int)
      Page.new(@document, index)
    end
  end
end
