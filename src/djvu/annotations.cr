module Djvu
  class Annotations
    def initialize(@exp : LibDjvu::MiniexpT)
    end

    {% for field in %i[bgcolor zoom mode horizalign vertalign] %}
      def {{field.id}}
        chars = Djvu.djvu_anno_get_{{field.id}}(@exp)
        return if chars.null?
        String.new(chars)
      end
    {% end %}

    def metadata
      Hash(String, String).new.tap do |hash|
        keys = LibDjvu.ddjvu_anno_get_metadata_keys(@exp)
        loop do
          key_exp = keys.value
          break if key_exp == LibDjvu::MINIEXP_NIL

          key = String.new(LibDjvu.miniexp_to_name(key_exp))
          value = String.new(LibDjvu.ddjvu_anno_get_metadata(@exp, key_exp))
          hash[key] = value
          keys += 1
        end
      end
    end
  end
end
