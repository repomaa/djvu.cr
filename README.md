# djvu.cr

This lib provides methods to inspect djvu documents

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     djvu:
       gitlab: repomaa/djvu.cr
   ```

2. Run `shards install`

## Usage

```crystal
require "djvu"

doc = Djvu::Document.new("path/to/file.djvu")
doc.annotations.try do |annotations|
  pp annotations.metadata
end

doc.pages.each do |page|
  pp page.text
  # or
  page.html(STDOUT)
end
```

## Contributing

1. Fork it (<https://gitlab.com/repomaa/djvu/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [Joakim Repomaa](https://gitlab.com/repomaa) - creator and maintainer
